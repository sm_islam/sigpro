import os
import pandas as pd
import sigProfilerPlotting as plot

filename= "."
filelist = []
count = 1
for file in os.listdir(filename):
    #print(count)
    df = pd.read_csv(filename+"/"+file)
    if not file == '.DS_Store':
        filelist.append(file)
# =============================================================================
#     if count==1:
#             genomes = df
#     else:
#          	genomes = pd.merge(genomes, df, on=["Mutation type", "Trinucleotide"])
#     count += 1
# =============================================================================
   
    
    
    


for i in filelist:
    file = i.split(".")[0]
    print (file)    
    
    genomes, index, colnames, mtypes = read_csv(file+".96.csv")
    
    processAvg = genomes
    
    processAvg= pd.DataFrame(processAvg)
    processes = processAvg.set_index(index)
    processes.columns = colnames
    processes = processes.rename_axis("signatures_plot", axis="columns")
    #print(processes)
    #print("process are ok", processes)
    processes.to_csv("signatures_plot.txt", "\t", index_label=[processes.columns.name]) 
    
    
    plot.plotSBS("signatures_plot.txt", "../pcawg/"+file+"/signatures_plot.txt", "", "96", percentage=False)import os
